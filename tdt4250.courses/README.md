Brief description of the main classes

- Course:
The course class has four <EAttribute> fields, namely the course code, name, content and the credits for the course. The course class references to other classes using the <EReference> "Courseinstance", "Department", "Course" (it's self) and "Studyprogram".
	 
- Faculty:
Class for faculty with <EAttribute> fields; name and abbreviation(IE etc.)
Can have multiple departments -> <EReference> "Department"

- Department:
Class for departments with <EAttribute> fields; name and abbreviation (IDI etc.).
Has reference to courses and studyprogram -> <EReference> "Course",  "Studyprogram"

- Studyprogram:
The class contains the <EAttribute> "code" for the study program (eg. MIT) and reference <EReference> to students who has that study program. 

- Courseinstance:
The instance of a Course. Course can consists of a number of course instances, one for each semester the course has been taught. Also a constraint of "courseHours" which checks that the number of hours for the course is correct, eg. number of lab hours and lecture hours = the combined timetable hours. See CoursesValidator.java -> validateCourseinstance_courseHours()

- Coursework:
Lecture and lab hours for this instance of the course

- Timetable:
Timetable with timetable entries (ttEntry)

- ttEntry:
<EAttribute> fields; "Day" (eg. monday, friday), "Time" ("10:15 - 12:00"), "Room" and "Type" (either lab or lecture)

- Evaluationform:
<EAttribute> fields; "Exam", "Assignments", "Project"
<EReference> to Student, so one student can have many evaluation forms corresponding to the courses he has

- Organisation:
<EReference> fields; "CourseCoordinater", "Lecturer", "CourseInstance"

- Person:
<EAttribute> fields; "Name"
Is superclass for "CourseCoordinater", "Lecturer" and "Student"

- Day:
 EEnumeration for the type of Day. A Day can be of type �Monday�, �Tuesday� etc. in the model. Used for the ttEntry <EAttribute> "Day"
 
- Type:
  EEnumeration for the type of Type. A Type can be "Lecture" and "Lab". Used for the ttEntry <EAttribute> "Type"


