/**
 */
package tdt4250.courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.Course#getCourseCode <em>Course Code</em>}</li>
 *   <li>{@link tdt4250.courses.Course#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.courses.Course#getContent <em>Content</em>}</li>
 *   <li>{@link tdt4250.courses.Course#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.courses.Course#getCourseInstances <em>Course Instances</em>}</li>
 *   <li>{@link tdt4250.courses.Course#getDepartment <em>Department</em>}</li>
 *   <li>{@link tdt4250.courses.Course#getRequiredcondition <em>Requiredcondition</em>}</li>
 *   <li>{@link tdt4250.courses.Course#getRecommendedcondition <em>Recommendedcondition</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Course Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Code</em>' attribute.
	 * @see #setCourseCode(String)
	 * @see tdt4250.courses.CoursesPackage#getCourse_CourseCode()
	 * @model
	 * @generated
	 */
	String getCourseCode();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Course#getCourseCode <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course Code</em>' attribute.
	 * @see #getCourseCode()
	 * @generated
	 */
	void setCourseCode(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see tdt4250.courses.CoursesPackage#getCourse_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Content</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Content</em>' attribute.
	 * @see #setContent(String)
	 * @see tdt4250.courses.CoursesPackage#getCourse_Content()
	 * @model
	 * @generated
	 */
	String getContent();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Course#getContent <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Content</em>' attribute.
	 * @see #getContent()
	 * @generated
	 */
	void setContent(String value);

	/**
	 * Returns the value of the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Credits</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Credits</em>' attribute.
	 * @see #setCredits(double)
	 * @see tdt4250.courses.CoursesPackage#getCourse_Credits()
	 * @model required="true"
	 * @generated
	 */
	double getCredits();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Course#getCredits <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Credits</em>' attribute.
	 * @see #getCredits()
	 * @generated
	 */
	void setCredits(double value);

	/**
	 * Returns the value of the '<em><b>Course Instances</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.courses.Courseinstance}.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Courseinstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course Instances</em>' containment reference list.
	 * @see tdt4250.courses.CoursesPackage#getCourse_CourseInstances()
	 * @see tdt4250.courses.Courseinstance#getCourse
	 * @model opposite="course" containment="true"
	 * @generated
	 */
	EList<Courseinstance> getCourseInstances();

	/**
	 * Returns the value of the '<em><b>Department</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Department</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department</em>' container reference.
	 * @see #setDepartment(Department)
	 * @see tdt4250.courses.CoursesPackage#getCourse_Department()
	 * @see tdt4250.courses.Department#getCourses
	 * @model opposite="courses" required="true" transient="false"
	 * @generated
	 */
	Department getDepartment();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Course#getDepartment <em>Department</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Department</em>' container reference.
	 * @see #getDepartment()
	 * @generated
	 */
	void setDepartment(Department value);

	/**
	 * Returns the value of the '<em><b>Requiredcondition</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.courses.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requiredcondition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requiredcondition</em>' reference list.
	 * @see tdt4250.courses.CoursesPackage#getCourse_Requiredcondition()
	 * @model
	 * @generated
	 */
	EList<Course> getRequiredcondition();

	/**
	 * Returns the value of the '<em><b>Recommendedcondition</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.courses.Studyprogram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recommendedcondition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recommendedcondition</em>' reference list.
	 * @see tdt4250.courses.CoursesPackage#getCourse_Recommendedcondition()
	 * @model
	 * @generated
	 */
	EList<Studyprogram> getRecommendedcondition();

} // Course
