/**
 */
package tdt4250.courses;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course Cordinator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.CourseCordinator#getOrganisitation <em>Organisitation</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getCourseCordinator()
 * @model
 * @generated
 */
public interface CourseCordinator extends Person {
	/**
	 * Returns the value of the '<em><b>Organisitation</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Organisation#getCoursecordinator <em>Coursecordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Organisitation</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Organisitation</em>' container reference.
	 * @see #setOrganisitation(Organisation)
	 * @see tdt4250.courses.CoursesPackage#getCourseCordinator_Organisitation()
	 * @see tdt4250.courses.Organisation#getCoursecordinator
	 * @model opposite="coursecordinator" required="true" transient="false"
	 * @generated
	 */
	Organisation getOrganisitation();

	/**
	 * Sets the value of the '{@link tdt4250.courses.CourseCordinator#getOrganisitation <em>Organisitation</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Organisitation</em>' container reference.
	 * @see #getOrganisitation()
	 * @generated
	 */
	void setOrganisitation(Organisation value);

} // CourseCordinator
