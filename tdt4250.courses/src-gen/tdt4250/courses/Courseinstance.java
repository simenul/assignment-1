/**
 */
package tdt4250.courses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Courseinstance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.Courseinstance#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.courses.Courseinstance#getOrganisation <em>Organisation</em>}</li>
 *   <li>{@link tdt4250.courses.Courseinstance#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link tdt4250.courses.Courseinstance#getCoursework <em>Coursework</em>}</li>
 *   <li>{@link tdt4250.courses.Courseinstance#getTimetable <em>Timetable</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getCourseinstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='courseHours'"
 * @generated
 */
public interface Courseinstance extends EObject {
	/**
	 * Returns the value of the '<em><b>Course</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Course#getCourseInstances <em>Course Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' container reference.
	 * @see #setCourse(Course)
	 * @see tdt4250.courses.CoursesPackage#getCourseinstance_Course()
	 * @see tdt4250.courses.Course#getCourseInstances
	 * @model opposite="courseInstances" required="true" transient="false"
	 * @generated
	 */
	Course getCourse();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Courseinstance#getCourse <em>Course</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Course</em>' container reference.
	 * @see #getCourse()
	 * @generated
	 */
	void setCourse(Course value);

	/**
	 * Returns the value of the '<em><b>Organisation</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Organisation#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Organisation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Organisation</em>' containment reference.
	 * @see #setOrganisation(Organisation)
	 * @see tdt4250.courses.CoursesPackage#getCourseinstance_Organisation()
	 * @see tdt4250.courses.Organisation#getCourseinstance
	 * @model opposite="courseinstance" containment="true" required="true"
	 * @generated
	 */
	Organisation getOrganisation();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Courseinstance#getOrganisation <em>Organisation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Organisation</em>' containment reference.
	 * @see #getOrganisation()
	 * @generated
	 */
	void setOrganisation(Organisation value);

	/**
	 * Returns the value of the '<em><b>Evaluationform</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Evaluationform#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluationform</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluationform</em>' containment reference.
	 * @see #setEvaluationform(Evaluationform)
	 * @see tdt4250.courses.CoursesPackage#getCourseinstance_Evaluationform()
	 * @see tdt4250.courses.Evaluationform#getCourseinstance
	 * @model opposite="courseinstance" containment="true" required="true"
	 * @generated
	 */
	Evaluationform getEvaluationform();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Courseinstance#getEvaluationform <em>Evaluationform</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Evaluationform</em>' containment reference.
	 * @see #getEvaluationform()
	 * @generated
	 */
	void setEvaluationform(Evaluationform value);

	/**
	 * Returns the value of the '<em><b>Coursework</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Coursework#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coursework</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coursework</em>' containment reference.
	 * @see #setCoursework(Coursework)
	 * @see tdt4250.courses.CoursesPackage#getCourseinstance_Coursework()
	 * @see tdt4250.courses.Coursework#getCourseinstance
	 * @model opposite="courseinstance" containment="true" required="true"
	 * @generated
	 */
	Coursework getCoursework();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Courseinstance#getCoursework <em>Coursework</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coursework</em>' containment reference.
	 * @see #getCoursework()
	 * @generated
	 */
	void setCoursework(Coursework value);

	/**
	 * Returns the value of the '<em><b>Timetable</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Timetable#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timetable</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timetable</em>' containment reference.
	 * @see #setTimetable(Timetable)
	 * @see tdt4250.courses.CoursesPackage#getCourseinstance_Timetable()
	 * @see tdt4250.courses.Timetable#getCourseinstance
	 * @model opposite="courseinstance" containment="true" required="true"
	 * @generated
	 */
	Timetable getTimetable();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Courseinstance#getTimetable <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Timetable</em>' containment reference.
	 * @see #getTimetable()
	 * @generated
	 */
	void setTimetable(Timetable value);

} // Courseinstance
