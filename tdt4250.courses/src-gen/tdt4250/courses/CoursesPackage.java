/**
 */
package tdt4250.courses;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see tdt4250.courses.CoursesFactory
 * @model kind="package"
 * @generated
 */
public interface CoursesPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "courses";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.example.org/courses";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "courses";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	CoursesPackage eINSTANCE = tdt4250.courses.impl.CoursesPackageImpl.init();

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.CourseImpl <em>Course</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.CourseImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getCourse()
	 * @generated
	 */
	int COURSE = 0;

	/**
	 * The feature id for the '<em><b>Course Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_CODE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__NAME = 1;

	/**
	 * The feature id for the '<em><b>Content</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CONTENT = 2;

	/**
	 * The feature id for the '<em><b>Credits</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__CREDITS = 3;

	/**
	 * The feature id for the '<em><b>Course Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__COURSE_INSTANCES = 4;

	/**
	 * The feature id for the '<em><b>Department</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__DEPARTMENT = 5;

	/**
	 * The feature id for the '<em><b>Requiredcondition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__REQUIREDCONDITION = 6;

	/**
	 * The feature id for the '<em><b>Recommendedcondition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE__RECOMMENDEDCONDITION = 7;

	/**
	 * The number of structural features of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_FEATURE_COUNT = 8;

	/**
	 * The number of operations of the '<em>Course</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.CourseinstanceImpl <em>Courseinstance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.CourseinstanceImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getCourseinstance()
	 * @generated
	 */
	int COURSEINSTANCE = 1;

	/**
	 * The feature id for the '<em><b>Course</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEINSTANCE__COURSE = 0;

	/**
	 * The feature id for the '<em><b>Organisation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEINSTANCE__ORGANISATION = 1;

	/**
	 * The feature id for the '<em><b>Evaluationform</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEINSTANCE__EVALUATIONFORM = 2;

	/**
	 * The feature id for the '<em><b>Coursework</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEINSTANCE__COURSEWORK = 3;

	/**
	 * The feature id for the '<em><b>Timetable</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEINSTANCE__TIMETABLE = 4;

	/**
	 * The number of structural features of the '<em>Courseinstance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEINSTANCE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Courseinstance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEINSTANCE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.OrganisationImpl <em>Organisation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.OrganisationImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getOrganisation()
	 * @generated
	 */
	int ORGANISATION = 2;

	/**
	 * The feature id for the '<em><b>Coursecordinator</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__COURSECORDINATOR = 0;

	/**
	 * The feature id for the '<em><b>Lecturer</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__LECTURER = 1;

	/**
	 * The feature id for the '<em><b>Courseinstance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__COURSEINSTANCE = 2;

	/**
	 * The number of structural features of the '<em>Organisation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Organisation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.DepartmentImpl <em>Department</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.DepartmentImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getDepartment()
	 * @generated
	 */
	int DEPARTMENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__NAME = 0;

	/**
	 * The feature id for the '<em><b>Abbreviation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__ABBREVIATION = 1;

	/**
	 * The feature id for the '<em><b>Courses</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__COURSES = 2;

	/**
	 * The feature id for the '<em><b>Studyprogram</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT__STUDYPROGRAM = 3;

	/**
	 * The number of structural features of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Department</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPARTMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.FacultyImpl <em>Faculty</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.FacultyImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getFaculty()
	 * @generated
	 */
	int FACULTY = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACULTY__NAME = 0;

	/**
	 * The feature id for the '<em><b>Abbreviation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACULTY__ABBREVIATION = 1;

	/**
	 * The feature id for the '<em><b>Departments</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACULTY__DEPARTMENTS = 2;

	/**
	 * The number of structural features of the '<em>Faculty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACULTY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Faculty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FACULTY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.EvaluationformImpl <em>Evaluationform</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.EvaluationformImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getEvaluationform()
	 * @generated
	 */
	int EVALUATIONFORM = 5;

	/**
	 * The feature id for the '<em><b>Exam</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATIONFORM__EXAM = 0;

	/**
	 * The feature id for the '<em><b>Assignments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATIONFORM__ASSIGNMENTS = 1;

	/**
	 * The feature id for the '<em><b>Project</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATIONFORM__PROJECT = 2;

	/**
	 * The feature id for the '<em><b>Courseinstance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATIONFORM__COURSEINSTANCE = 3;

	/**
	 * The feature id for the '<em><b>Student</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATIONFORM__STUDENT = 4;

	/**
	 * The number of structural features of the '<em>Evaluationform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATIONFORM_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Evaluationform</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVALUATIONFORM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.CourseworkImpl <em>Coursework</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.CourseworkImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getCoursework()
	 * @generated
	 */
	int COURSEWORK = 6;

	/**
	 * The feature id for the '<em><b>Lecturehours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK__LECTUREHOURS = 0;

	/**
	 * The feature id for the '<em><b>Labhours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK__LABHOURS = 1;

	/**
	 * The feature id for the '<em><b>Courseinstance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK__COURSEINSTANCE = 2;

	/**
	 * The number of structural features of the '<em>Coursework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Coursework</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSEWORK_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.PersonImpl <em>Person</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.PersonImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getPerson()
	 * @generated
	 */
	int PERSON = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON__NAME = 0;

	/**
	 * The number of structural features of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Person</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERSON_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.StudyprogramImpl <em>Studyprogram</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.StudyprogramImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getStudyprogram()
	 * @generated
	 */
	int STUDYPROGRAM = 8;

	/**
	 * The feature id for the '<em><b>Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDYPROGRAM__CODE = 0;

	/**
	 * The feature id for the '<em><b>Students</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDYPROGRAM__STUDENTS = 1;

	/**
	 * The number of structural features of the '<em>Studyprogram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDYPROGRAM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Studyprogram</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDYPROGRAM_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.TimetableImpl <em>Timetable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.TimetableImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getTimetable()
	 * @generated
	 */
	int TIMETABLE = 9;

	/**
	 * The feature id for the '<em><b>Courseinstance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__COURSEINSTANCE = 0;

	/**
	 * The feature id for the '<em><b>Ttentry</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE__TTENTRY = 1;

	/**
	 * The number of structural features of the '<em>Timetable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Timetable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMETABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.ttEntryImpl <em>tt Entry</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.ttEntryImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getttEntry()
	 * @generated
	 */
	int TT_ENTRY = 10;

	/**
	 * The feature id for the '<em><b>Day</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TT_ENTRY__DAY = 0;

	/**
	 * The feature id for the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TT_ENTRY__TIME = 1;

	/**
	 * The feature id for the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TT_ENTRY__ROOM = 2;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TT_ENTRY__TYPE = 3;

	/**
	 * The feature id for the '<em><b>Studyprogram</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TT_ENTRY__STUDYPROGRAM = 4;

	/**
	 * The number of structural features of the '<em>tt Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TT_ENTRY_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>tt Entry</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TT_ENTRY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.StudentImpl <em>Student</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.StudentImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getStudent()
	 * @generated
	 */
	int STUDENT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT__NAME = PERSON__NAME;

	/**
	 * The feature id for the '<em><b>Studyprogram</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT__STUDYPROGRAM = PERSON_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Evaluation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT__EVALUATION = PERSON_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Student</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_FEATURE_COUNT = PERSON_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Student</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STUDENT_OPERATION_COUNT = PERSON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.CourseCordinatorImpl <em>Course Cordinator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.CourseCordinatorImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getCourseCordinator()
	 * @generated
	 */
	int COURSE_CORDINATOR = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CORDINATOR__NAME = PERSON__NAME;

	/**
	 * The feature id for the '<em><b>Organisitation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CORDINATOR__ORGANISITATION = PERSON_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Course Cordinator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CORDINATOR_FEATURE_COUNT = PERSON_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Course Cordinator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COURSE_CORDINATOR_OPERATION_COUNT = PERSON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.impl.LecturerImpl <em>Lecturer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.impl.LecturerImpl
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getLecturer()
	 * @generated
	 */
	int LECTURER = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURER__NAME = PERSON__NAME;

	/**
	 * The feature id for the '<em><b>Organisation</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURER__ORGANISATION = PERSON_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Lecturer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURER_FEATURE_COUNT = PERSON_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Lecturer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LECTURER_OPERATION_COUNT = PERSON_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link tdt4250.courses.Day <em>Day</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.Day
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getDay()
	 * @generated
	 */
	int DAY = 14;

	/**
	 * The meta object id for the '{@link tdt4250.courses.Type <em>Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see tdt4250.courses.Type
	 * @see tdt4250.courses.impl.CoursesPackageImpl#getType()
	 * @generated
	 */
	int TYPE = 15;

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Course <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course</em>'.
	 * @see tdt4250.courses.Course
	 * @generated
	 */
	EClass getCourse();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Course#getCourseCode <em>Course Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Course Code</em>'.
	 * @see tdt4250.courses.Course#getCourseCode()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_CourseCode();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Course#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.courses.Course#getName()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Course#getContent <em>Content</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Content</em>'.
	 * @see tdt4250.courses.Course#getContent()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Content();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Course#getCredits <em>Credits</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Credits</em>'.
	 * @see tdt4250.courses.Course#getCredits()
	 * @see #getCourse()
	 * @generated
	 */
	EAttribute getCourse_Credits();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.courses.Course#getCourseInstances <em>Course Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Course Instances</em>'.
	 * @see tdt4250.courses.Course#getCourseInstances()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_CourseInstances();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.Course#getDepartment <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Department</em>'.
	 * @see tdt4250.courses.Course#getDepartment()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Department();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.courses.Course#getRequiredcondition <em>Requiredcondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Requiredcondition</em>'.
	 * @see tdt4250.courses.Course#getRequiredcondition()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Requiredcondition();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.courses.Course#getRecommendedcondition <em>Recommendedcondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Recommendedcondition</em>'.
	 * @see tdt4250.courses.Course#getRecommendedcondition()
	 * @see #getCourse()
	 * @generated
	 */
	EReference getCourse_Recommendedcondition();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Courseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Courseinstance</em>'.
	 * @see tdt4250.courses.Courseinstance
	 * @generated
	 */
	EClass getCourseinstance();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.Courseinstance#getCourse <em>Course</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Course</em>'.
	 * @see tdt4250.courses.Courseinstance#getCourse()
	 * @see #getCourseinstance()
	 * @generated
	 */
	EReference getCourseinstance_Course();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.courses.Courseinstance#getOrganisation <em>Organisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Organisation</em>'.
	 * @see tdt4250.courses.Courseinstance#getOrganisation()
	 * @see #getCourseinstance()
	 * @generated
	 */
	EReference getCourseinstance_Organisation();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.courses.Courseinstance#getEvaluationform <em>Evaluationform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Evaluationform</em>'.
	 * @see tdt4250.courses.Courseinstance#getEvaluationform()
	 * @see #getCourseinstance()
	 * @generated
	 */
	EReference getCourseinstance_Evaluationform();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.courses.Courseinstance#getCoursework <em>Coursework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Coursework</em>'.
	 * @see tdt4250.courses.Courseinstance#getCoursework()
	 * @see #getCourseinstance()
	 * @generated
	 */
	EReference getCourseinstance_Coursework();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.courses.Courseinstance#getTimetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Timetable</em>'.
	 * @see tdt4250.courses.Courseinstance#getTimetable()
	 * @see #getCourseinstance()
	 * @generated
	 */
	EReference getCourseinstance_Timetable();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Organisation <em>Organisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organisation</em>'.
	 * @see tdt4250.courses.Organisation
	 * @generated
	 */
	EClass getOrganisation();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.courses.Organisation#getCoursecordinator <em>Coursecordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Coursecordinator</em>'.
	 * @see tdt4250.courses.Organisation#getCoursecordinator()
	 * @see #getOrganisation()
	 * @generated
	 */
	EReference getOrganisation_Coursecordinator();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.courses.Organisation#getLecturer <em>Lecturer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Lecturer</em>'.
	 * @see tdt4250.courses.Organisation#getLecturer()
	 * @see #getOrganisation()
	 * @generated
	 */
	EReference getOrganisation_Lecturer();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.Organisation#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Courseinstance</em>'.
	 * @see tdt4250.courses.Organisation#getCourseinstance()
	 * @see #getOrganisation()
	 * @generated
	 */
	EReference getOrganisation_Courseinstance();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Department <em>Department</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Department</em>'.
	 * @see tdt4250.courses.Department
	 * @generated
	 */
	EClass getDepartment();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Department#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.courses.Department#getName()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Department#getAbbreviation <em>Abbreviation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviation</em>'.
	 * @see tdt4250.courses.Department#getAbbreviation()
	 * @see #getDepartment()
	 * @generated
	 */
	EAttribute getDepartment_Abbreviation();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.courses.Department#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Courses</em>'.
	 * @see tdt4250.courses.Department#getCourses()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Courses();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.courses.Department#getStudyprogram <em>Studyprogram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Studyprogram</em>'.
	 * @see tdt4250.courses.Department#getStudyprogram()
	 * @see #getDepartment()
	 * @generated
	 */
	EReference getDepartment_Studyprogram();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Faculty <em>Faculty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Faculty</em>'.
	 * @see tdt4250.courses.Faculty
	 * @generated
	 */
	EClass getFaculty();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Faculty#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.courses.Faculty#getName()
	 * @see #getFaculty()
	 * @generated
	 */
	EAttribute getFaculty_Name();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Faculty#getAbbreviation <em>Abbreviation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abbreviation</em>'.
	 * @see tdt4250.courses.Faculty#getAbbreviation()
	 * @see #getFaculty()
	 * @generated
	 */
	EAttribute getFaculty_Abbreviation();

	/**
	 * Returns the meta object for the containment reference '{@link tdt4250.courses.Faculty#getDepartments <em>Departments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Departments</em>'.
	 * @see tdt4250.courses.Faculty#getDepartments()
	 * @see #getFaculty()
	 * @generated
	 */
	EReference getFaculty_Departments();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Evaluationform <em>Evaluationform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Evaluationform</em>'.
	 * @see tdt4250.courses.Evaluationform
	 * @generated
	 */
	EClass getEvaluationform();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Evaluationform#getExam <em>Exam</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exam</em>'.
	 * @see tdt4250.courses.Evaluationform#getExam()
	 * @see #getEvaluationform()
	 * @generated
	 */
	EAttribute getEvaluationform_Exam();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Evaluationform#getAssignments <em>Assignments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assignments</em>'.
	 * @see tdt4250.courses.Evaluationform#getAssignments()
	 * @see #getEvaluationform()
	 * @generated
	 */
	EAttribute getEvaluationform_Assignments();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Evaluationform#getProject <em>Project</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Project</em>'.
	 * @see tdt4250.courses.Evaluationform#getProject()
	 * @see #getEvaluationform()
	 * @generated
	 */
	EAttribute getEvaluationform_Project();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.Evaluationform#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Courseinstance</em>'.
	 * @see tdt4250.courses.Evaluationform#getCourseinstance()
	 * @see #getEvaluationform()
	 * @generated
	 */
	EReference getEvaluationform_Courseinstance();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.courses.Evaluationform#getStudent <em>Student</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Student</em>'.
	 * @see tdt4250.courses.Evaluationform#getStudent()
	 * @see #getEvaluationform()
	 * @generated
	 */
	EReference getEvaluationform_Student();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Coursework <em>Coursework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Coursework</em>'.
	 * @see tdt4250.courses.Coursework
	 * @generated
	 */
	EClass getCoursework();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Coursework#getLecturehours <em>Lecturehours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lecturehours</em>'.
	 * @see tdt4250.courses.Coursework#getLecturehours()
	 * @see #getCoursework()
	 * @generated
	 */
	EAttribute getCoursework_Lecturehours();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Coursework#getLabhours <em>Labhours</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Labhours</em>'.
	 * @see tdt4250.courses.Coursework#getLabhours()
	 * @see #getCoursework()
	 * @generated
	 */
	EAttribute getCoursework_Labhours();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.Coursework#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Courseinstance</em>'.
	 * @see tdt4250.courses.Coursework#getCourseinstance()
	 * @see #getCoursework()
	 * @generated
	 */
	EReference getCoursework_Courseinstance();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Person <em>Person</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Person</em>'.
	 * @see tdt4250.courses.Person
	 * @generated
	 */
	EClass getPerson();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Person#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see tdt4250.courses.Person#getName()
	 * @see #getPerson()
	 * @generated
	 */
	EAttribute getPerson_Name();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Studyprogram <em>Studyprogram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Studyprogram</em>'.
	 * @see tdt4250.courses.Studyprogram
	 * @generated
	 */
	EClass getStudyprogram();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.Studyprogram#getCode <em>Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Code</em>'.
	 * @see tdt4250.courses.Studyprogram#getCode()
	 * @see #getStudyprogram()
	 * @generated
	 */
	EAttribute getStudyprogram_Code();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.courses.Studyprogram#getStudents <em>Students</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Students</em>'.
	 * @see tdt4250.courses.Studyprogram#getStudents()
	 * @see #getStudyprogram()
	 * @generated
	 */
	EReference getStudyprogram_Students();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Timetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timetable</em>'.
	 * @see tdt4250.courses.Timetable
	 * @generated
	 */
	EClass getTimetable();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.Timetable#getCourseinstance <em>Courseinstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Courseinstance</em>'.
	 * @see tdt4250.courses.Timetable#getCourseinstance()
	 * @see #getTimetable()
	 * @generated
	 */
	EReference getTimetable_Courseinstance();

	/**
	 * Returns the meta object for the containment reference list '{@link tdt4250.courses.Timetable#getTtentry <em>Ttentry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Ttentry</em>'.
	 * @see tdt4250.courses.Timetable#getTtentry()
	 * @see #getTimetable()
	 * @generated
	 */
	EReference getTimetable_Ttentry();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.ttEntry <em>tt Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>tt Entry</em>'.
	 * @see tdt4250.courses.ttEntry
	 * @generated
	 */
	EClass getttEntry();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.ttEntry#getDay <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Day</em>'.
	 * @see tdt4250.courses.ttEntry#getDay()
	 * @see #getttEntry()
	 * @generated
	 */
	EAttribute getttEntry_Day();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.ttEntry#getTime <em>Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time</em>'.
	 * @see tdt4250.courses.ttEntry#getTime()
	 * @see #getttEntry()
	 * @generated
	 */
	EAttribute getttEntry_Time();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.ttEntry#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room</em>'.
	 * @see tdt4250.courses.ttEntry#getRoom()
	 * @see #getttEntry()
	 * @generated
	 */
	EAttribute getttEntry_Room();

	/**
	 * Returns the meta object for the attribute '{@link tdt4250.courses.ttEntry#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see tdt4250.courses.ttEntry#getType()
	 * @see #getttEntry()
	 * @generated
	 */
	EAttribute getttEntry_Type();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.courses.ttEntry#getStudyprogram <em>Studyprogram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Studyprogram</em>'.
	 * @see tdt4250.courses.ttEntry#getStudyprogram()
	 * @see #getttEntry()
	 * @generated
	 */
	EReference getttEntry_Studyprogram();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Student <em>Student</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Student</em>'.
	 * @see tdt4250.courses.Student
	 * @generated
	 */
	EClass getStudent();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.Student#getStudyprogram <em>Studyprogram</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Studyprogram</em>'.
	 * @see tdt4250.courses.Student#getStudyprogram()
	 * @see #getStudent()
	 * @generated
	 */
	EReference getStudent_Studyprogram();

	/**
	 * Returns the meta object for the reference list '{@link tdt4250.courses.Student#getEvaluation <em>Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Evaluation</em>'.
	 * @see tdt4250.courses.Student#getEvaluation()
	 * @see #getStudent()
	 * @generated
	 */
	EReference getStudent_Evaluation();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.CourseCordinator <em>Course Cordinator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Course Cordinator</em>'.
	 * @see tdt4250.courses.CourseCordinator
	 * @generated
	 */
	EClass getCourseCordinator();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.CourseCordinator#getOrganisitation <em>Organisitation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Organisitation</em>'.
	 * @see tdt4250.courses.CourseCordinator#getOrganisitation()
	 * @see #getCourseCordinator()
	 * @generated
	 */
	EReference getCourseCordinator_Organisitation();

	/**
	 * Returns the meta object for class '{@link tdt4250.courses.Lecturer <em>Lecturer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Lecturer</em>'.
	 * @see tdt4250.courses.Lecturer
	 * @generated
	 */
	EClass getLecturer();

	/**
	 * Returns the meta object for the container reference '{@link tdt4250.courses.Lecturer#getOrganisation <em>Organisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Organisation</em>'.
	 * @see tdt4250.courses.Lecturer#getOrganisation()
	 * @see #getLecturer()
	 * @generated
	 */
	EReference getLecturer_Organisation();

	/**
	 * Returns the meta object for enum '{@link tdt4250.courses.Day <em>Day</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Day</em>'.
	 * @see tdt4250.courses.Day
	 * @generated
	 */
	EEnum getDay();

	/**
	 * Returns the meta object for enum '{@link tdt4250.courses.Type <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Type</em>'.
	 * @see tdt4250.courses.Type
	 * @generated
	 */
	EEnum getType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	CoursesFactory getCoursesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.CourseImpl <em>Course</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.CourseImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getCourse()
		 * @generated
		 */
		EClass COURSE = eINSTANCE.getCourse();

		/**
		 * The meta object literal for the '<em><b>Course Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__COURSE_CODE = eINSTANCE.getCourse_CourseCode();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__NAME = eINSTANCE.getCourse_Name();

		/**
		 * The meta object literal for the '<em><b>Content</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CONTENT = eINSTANCE.getCourse_Content();

		/**
		 * The meta object literal for the '<em><b>Credits</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSE__CREDITS = eINSTANCE.getCourse_Credits();

		/**
		 * The meta object literal for the '<em><b>Course Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__COURSE_INSTANCES = eINSTANCE.getCourse_CourseInstances();

		/**
		 * The meta object literal for the '<em><b>Department</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__DEPARTMENT = eINSTANCE.getCourse_Department();

		/**
		 * The meta object literal for the '<em><b>Requiredcondition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__REQUIREDCONDITION = eINSTANCE.getCourse_Requiredcondition();

		/**
		 * The meta object literal for the '<em><b>Recommendedcondition</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE__RECOMMENDEDCONDITION = eINSTANCE.getCourse_Recommendedcondition();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.CourseinstanceImpl <em>Courseinstance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.CourseinstanceImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getCourseinstance()
		 * @generated
		 */
		EClass COURSEINSTANCE = eINSTANCE.getCourseinstance();

		/**
		 * The meta object literal for the '<em><b>Course</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSEINSTANCE__COURSE = eINSTANCE.getCourseinstance_Course();

		/**
		 * The meta object literal for the '<em><b>Organisation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSEINSTANCE__ORGANISATION = eINSTANCE.getCourseinstance_Organisation();

		/**
		 * The meta object literal for the '<em><b>Evaluationform</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSEINSTANCE__EVALUATIONFORM = eINSTANCE.getCourseinstance_Evaluationform();

		/**
		 * The meta object literal for the '<em><b>Coursework</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSEINSTANCE__COURSEWORK = eINSTANCE.getCourseinstance_Coursework();

		/**
		 * The meta object literal for the '<em><b>Timetable</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSEINSTANCE__TIMETABLE = eINSTANCE.getCourseinstance_Timetable();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.OrganisationImpl <em>Organisation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.OrganisationImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getOrganisation()
		 * @generated
		 */
		EClass ORGANISATION = eINSTANCE.getOrganisation();

		/**
		 * The meta object literal for the '<em><b>Coursecordinator</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION__COURSECORDINATOR = eINSTANCE.getOrganisation_Coursecordinator();

		/**
		 * The meta object literal for the '<em><b>Lecturer</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION__LECTURER = eINSTANCE.getOrganisation_Lecturer();

		/**
		 * The meta object literal for the '<em><b>Courseinstance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION__COURSEINSTANCE = eINSTANCE.getOrganisation_Courseinstance();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.DepartmentImpl <em>Department</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.DepartmentImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getDepartment()
		 * @generated
		 */
		EClass DEPARTMENT = eINSTANCE.getDepartment();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__NAME = eINSTANCE.getDepartment_Name();

		/**
		 * The meta object literal for the '<em><b>Abbreviation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DEPARTMENT__ABBREVIATION = eINSTANCE.getDepartment_Abbreviation();

		/**
		 * The meta object literal for the '<em><b>Courses</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__COURSES = eINSTANCE.getDepartment_Courses();

		/**
		 * The meta object literal for the '<em><b>Studyprogram</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPARTMENT__STUDYPROGRAM = eINSTANCE.getDepartment_Studyprogram();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.FacultyImpl <em>Faculty</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.FacultyImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getFaculty()
		 * @generated
		 */
		EClass FACULTY = eINSTANCE.getFaculty();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FACULTY__NAME = eINSTANCE.getFaculty_Name();

		/**
		 * The meta object literal for the '<em><b>Abbreviation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FACULTY__ABBREVIATION = eINSTANCE.getFaculty_Abbreviation();

		/**
		 * The meta object literal for the '<em><b>Departments</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FACULTY__DEPARTMENTS = eINSTANCE.getFaculty_Departments();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.EvaluationformImpl <em>Evaluationform</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.EvaluationformImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getEvaluationform()
		 * @generated
		 */
		EClass EVALUATIONFORM = eINSTANCE.getEvaluationform();

		/**
		 * The meta object literal for the '<em><b>Exam</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATIONFORM__EXAM = eINSTANCE.getEvaluationform_Exam();

		/**
		 * The meta object literal for the '<em><b>Assignments</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATIONFORM__ASSIGNMENTS = eINSTANCE.getEvaluationform_Assignments();

		/**
		 * The meta object literal for the '<em><b>Project</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVALUATIONFORM__PROJECT = eINSTANCE.getEvaluationform_Project();

		/**
		 * The meta object literal for the '<em><b>Courseinstance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATIONFORM__COURSEINSTANCE = eINSTANCE.getEvaluationform_Courseinstance();

		/**
		 * The meta object literal for the '<em><b>Student</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVALUATIONFORM__STUDENT = eINSTANCE.getEvaluationform_Student();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.CourseworkImpl <em>Coursework</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.CourseworkImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getCoursework()
		 * @generated
		 */
		EClass COURSEWORK = eINSTANCE.getCoursework();

		/**
		 * The meta object literal for the '<em><b>Lecturehours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSEWORK__LECTUREHOURS = eINSTANCE.getCoursework_Lecturehours();

		/**
		 * The meta object literal for the '<em><b>Labhours</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COURSEWORK__LABHOURS = eINSTANCE.getCoursework_Labhours();

		/**
		 * The meta object literal for the '<em><b>Courseinstance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSEWORK__COURSEINSTANCE = eINSTANCE.getCoursework_Courseinstance();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.PersonImpl <em>Person</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.PersonImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getPerson()
		 * @generated
		 */
		EClass PERSON = eINSTANCE.getPerson();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERSON__NAME = eINSTANCE.getPerson_Name();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.StudyprogramImpl <em>Studyprogram</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.StudyprogramImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getStudyprogram()
		 * @generated
		 */
		EClass STUDYPROGRAM = eINSTANCE.getStudyprogram();

		/**
		 * The meta object literal for the '<em><b>Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute STUDYPROGRAM__CODE = eINSTANCE.getStudyprogram_Code();

		/**
		 * The meta object literal for the '<em><b>Students</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDYPROGRAM__STUDENTS = eINSTANCE.getStudyprogram_Students();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.TimetableImpl <em>Timetable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.TimetableImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getTimetable()
		 * @generated
		 */
		EClass TIMETABLE = eINSTANCE.getTimetable();

		/**
		 * The meta object literal for the '<em><b>Courseinstance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE__COURSEINSTANCE = eINSTANCE.getTimetable_Courseinstance();

		/**
		 * The meta object literal for the '<em><b>Ttentry</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMETABLE__TTENTRY = eINSTANCE.getTimetable_Ttentry();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.ttEntryImpl <em>tt Entry</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.ttEntryImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getttEntry()
		 * @generated
		 */
		EClass TT_ENTRY = eINSTANCE.getttEntry();

		/**
		 * The meta object literal for the '<em><b>Day</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TT_ENTRY__DAY = eINSTANCE.getttEntry_Day();

		/**
		 * The meta object literal for the '<em><b>Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TT_ENTRY__TIME = eINSTANCE.getttEntry_Time();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TT_ENTRY__ROOM = eINSTANCE.getttEntry_Room();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TT_ENTRY__TYPE = eINSTANCE.getttEntry_Type();

		/**
		 * The meta object literal for the '<em><b>Studyprogram</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TT_ENTRY__STUDYPROGRAM = eINSTANCE.getttEntry_Studyprogram();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.StudentImpl <em>Student</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.StudentImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getStudent()
		 * @generated
		 */
		EClass STUDENT = eINSTANCE.getStudent();

		/**
		 * The meta object literal for the '<em><b>Studyprogram</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDENT__STUDYPROGRAM = eINSTANCE.getStudent_Studyprogram();

		/**
		 * The meta object literal for the '<em><b>Evaluation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STUDENT__EVALUATION = eINSTANCE.getStudent_Evaluation();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.CourseCordinatorImpl <em>Course Cordinator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.CourseCordinatorImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getCourseCordinator()
		 * @generated
		 */
		EClass COURSE_CORDINATOR = eINSTANCE.getCourseCordinator();

		/**
		 * The meta object literal for the '<em><b>Organisitation</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COURSE_CORDINATOR__ORGANISITATION = eINSTANCE.getCourseCordinator_Organisitation();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.impl.LecturerImpl <em>Lecturer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.impl.LecturerImpl
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getLecturer()
		 * @generated
		 */
		EClass LECTURER = eINSTANCE.getLecturer();

		/**
		 * The meta object literal for the '<em><b>Organisation</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LECTURER__ORGANISATION = eINSTANCE.getLecturer_Organisation();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.Day <em>Day</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.Day
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getDay()
		 * @generated
		 */
		EEnum DAY = eINSTANCE.getDay();

		/**
		 * The meta object literal for the '{@link tdt4250.courses.Type <em>Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see tdt4250.courses.Type
		 * @see tdt4250.courses.impl.CoursesPackageImpl#getType()
		 * @generated
		 */
		EEnum TYPE = eINSTANCE.getType();

	}

} //CoursesPackage
