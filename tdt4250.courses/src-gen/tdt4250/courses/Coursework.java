/**
 */
package tdt4250.courses;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Coursework</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.Coursework#getLecturehours <em>Lecturehours</em>}</li>
 *   <li>{@link tdt4250.courses.Coursework#getLabhours <em>Labhours</em>}</li>
 *   <li>{@link tdt4250.courses.Coursework#getCourseinstance <em>Courseinstance</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getCoursework()
 * @model
 * @generated
 */
public interface Coursework extends EObject {
	/**
	 * Returns the value of the '<em><b>Lecturehours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecturehours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecturehours</em>' attribute.
	 * @see #setLecturehours(int)
	 * @see tdt4250.courses.CoursesPackage#getCoursework_Lecturehours()
	 * @model required="true"
	 * @generated
	 */
	int getLecturehours();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Coursework#getLecturehours <em>Lecturehours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lecturehours</em>' attribute.
	 * @see #getLecturehours()
	 * @generated
	 */
	void setLecturehours(int value);

	/**
	 * Returns the value of the '<em><b>Labhours</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Labhours</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Labhours</em>' attribute.
	 * @see #setLabhours(int)
	 * @see tdt4250.courses.CoursesPackage#getCoursework_Labhours()
	 * @model
	 * @generated
	 */
	int getLabhours();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Coursework#getLabhours <em>Labhours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Labhours</em>' attribute.
	 * @see #getLabhours()
	 * @generated
	 */
	void setLabhours(int value);

	/**
	 * Returns the value of the '<em><b>Courseinstance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Courseinstance#getCoursework <em>Coursework</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courseinstance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courseinstance</em>' container reference.
	 * @see #setCourseinstance(Courseinstance)
	 * @see tdt4250.courses.CoursesPackage#getCoursework_Courseinstance()
	 * @see tdt4250.courses.Courseinstance#getCoursework
	 * @model opposite="coursework" required="true" transient="false"
	 * @generated
	 */
	Courseinstance getCourseinstance();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Coursework#getCourseinstance <em>Courseinstance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Courseinstance</em>' container reference.
	 * @see #getCourseinstance()
	 * @generated
	 */
	void setCourseinstance(Courseinstance value);

} // Coursework
