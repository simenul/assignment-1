/**
 */
package tdt4250.courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Evaluationform</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.Evaluationform#getExam <em>Exam</em>}</li>
 *   <li>{@link tdt4250.courses.Evaluationform#getAssignments <em>Assignments</em>}</li>
 *   <li>{@link tdt4250.courses.Evaluationform#getProject <em>Project</em>}</li>
 *   <li>{@link tdt4250.courses.Evaluationform#getCourseinstance <em>Courseinstance</em>}</li>
 *   <li>{@link tdt4250.courses.Evaluationform#getStudent <em>Student</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getEvaluationform()
 * @model
 * @generated
 */
public interface Evaluationform extends EObject {
	/**
	 * Returns the value of the '<em><b>Exam</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exam</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exam</em>' attribute.
	 * @see #setExam(int)
	 * @see tdt4250.courses.CoursesPackage#getEvaluationform_Exam()
	 * @model required="true"
	 * @generated
	 */
	int getExam();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Evaluationform#getExam <em>Exam</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exam</em>' attribute.
	 * @see #getExam()
	 * @generated
	 */
	void setExam(int value);

	/**
	 * Returns the value of the '<em><b>Assignments</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignments</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignments</em>' attribute.
	 * @see #setAssignments(int)
	 * @see tdt4250.courses.CoursesPackage#getEvaluationform_Assignments()
	 * @model required="true"
	 * @generated
	 */
	int getAssignments();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Evaluationform#getAssignments <em>Assignments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignments</em>' attribute.
	 * @see #getAssignments()
	 * @generated
	 */
	void setAssignments(int value);

	/**
	 * Returns the value of the '<em><b>Project</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Project</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Project</em>' attribute.
	 * @see #setProject(int)
	 * @see tdt4250.courses.CoursesPackage#getEvaluationform_Project()
	 * @model required="true"
	 * @generated
	 */
	int getProject();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Evaluationform#getProject <em>Project</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Project</em>' attribute.
	 * @see #getProject()
	 * @generated
	 */
	void setProject(int value);

	/**
	 * Returns the value of the '<em><b>Courseinstance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Courseinstance#getEvaluationform <em>Evaluationform</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courseinstance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courseinstance</em>' container reference.
	 * @see #setCourseinstance(Courseinstance)
	 * @see tdt4250.courses.CoursesPackage#getEvaluationform_Courseinstance()
	 * @see tdt4250.courses.Courseinstance#getEvaluationform
	 * @model opposite="evaluationform" transient="false"
	 * @generated
	 */
	Courseinstance getCourseinstance();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Evaluationform#getCourseinstance <em>Courseinstance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Courseinstance</em>' container reference.
	 * @see #getCourseinstance()
	 * @generated
	 */
	void setCourseinstance(Courseinstance value);

	/**
	 * Returns the value of the '<em><b>Student</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.courses.Student}.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Student#getEvaluation <em>Evaluation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Student</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Student</em>' reference list.
	 * @see tdt4250.courses.CoursesPackage#getEvaluationform_Student()
	 * @see tdt4250.courses.Student#getEvaluation
	 * @model opposite="evaluation"
	 * @generated
	 */
	EList<Student> getStudent();

} // Evaluationform
