/**
 */
package tdt4250.courses;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Lecturer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.Lecturer#getOrganisation <em>Organisation</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getLecturer()
 * @model
 * @generated
 */
public interface Lecturer extends Person {
	/**
	 * Returns the value of the '<em><b>Organisation</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Organisation#getLecturer <em>Lecturer</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Organisation</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Organisation</em>' container reference.
	 * @see #setOrganisation(Organisation)
	 * @see tdt4250.courses.CoursesPackage#getLecturer_Organisation()
	 * @see tdt4250.courses.Organisation#getLecturer
	 * @model opposite="lecturer" required="true" transient="false"
	 * @generated
	 */
	Organisation getOrganisation();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Lecturer#getOrganisation <em>Organisation</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Organisation</em>' container reference.
	 * @see #getOrganisation()
	 * @generated
	 */
	void setOrganisation(Organisation value);

} // Lecturer
