/**
 */
package tdt4250.courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Organisation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.Organisation#getCoursecordinator <em>Coursecordinator</em>}</li>
 *   <li>{@link tdt4250.courses.Organisation#getLecturer <em>Lecturer</em>}</li>
 *   <li>{@link tdt4250.courses.Organisation#getCourseinstance <em>Courseinstance</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getOrganisation()
 * @model
 * @generated
 */
public interface Organisation extends EObject {
	/**
	 * Returns the value of the '<em><b>Coursecordinator</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.CourseCordinator#getOrganisitation <em>Organisitation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coursecordinator</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coursecordinator</em>' containment reference.
	 * @see #setCoursecordinator(CourseCordinator)
	 * @see tdt4250.courses.CoursesPackage#getOrganisation_Coursecordinator()
	 * @see tdt4250.courses.CourseCordinator#getOrganisitation
	 * @model opposite="organisitation" containment="true" required="true"
	 * @generated
	 */
	CourseCordinator getCoursecordinator();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Organisation#getCoursecordinator <em>Coursecordinator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coursecordinator</em>' containment reference.
	 * @see #getCoursecordinator()
	 * @generated
	 */
	void setCoursecordinator(CourseCordinator value);

	/**
	 * Returns the value of the '<em><b>Lecturer</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.courses.Lecturer}.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Lecturer#getOrganisation <em>Organisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lecturer</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lecturer</em>' containment reference list.
	 * @see tdt4250.courses.CoursesPackage#getOrganisation_Lecturer()
	 * @see tdt4250.courses.Lecturer#getOrganisation
	 * @model opposite="organisation" containment="true" required="true"
	 * @generated
	 */
	EList<Lecturer> getLecturer();

	/**
	 * Returns the value of the '<em><b>Courseinstance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Courseinstance#getOrganisation <em>Organisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courseinstance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courseinstance</em>' container reference.
	 * @see #setCourseinstance(Courseinstance)
	 * @see tdt4250.courses.CoursesPackage#getOrganisation_Courseinstance()
	 * @see tdt4250.courses.Courseinstance#getOrganisation
	 * @model opposite="organisation" transient="false"
	 * @generated
	 */
	Courseinstance getCourseinstance();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Organisation#getCourseinstance <em>Courseinstance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Courseinstance</em>' container reference.
	 * @see #getCourseinstance()
	 * @generated
	 */
	void setCourseinstance(Courseinstance value);

} // Organisation
