/**
 */
package tdt4250.courses;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Student</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.Student#getStudyprogram <em>Studyprogram</em>}</li>
 *   <li>{@link tdt4250.courses.Student#getEvaluation <em>Evaluation</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getStudent()
 * @model
 * @generated
 */
public interface Student extends Person {
	/**
	 * Returns the value of the '<em><b>Studyprogram</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Studyprogram#getStudents <em>Students</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Studyprogram</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Studyprogram</em>' container reference.
	 * @see #setStudyprogram(Studyprogram)
	 * @see tdt4250.courses.CoursesPackage#getStudent_Studyprogram()
	 * @see tdt4250.courses.Studyprogram#getStudents
	 * @model opposite="students" required="true" transient="false"
	 * @generated
	 */
	Studyprogram getStudyprogram();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Student#getStudyprogram <em>Studyprogram</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Studyprogram</em>' container reference.
	 * @see #getStudyprogram()
	 * @generated
	 */
	void setStudyprogram(Studyprogram value);

	/**
	 * Returns the value of the '<em><b>Evaluation</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.courses.Evaluationform}.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Evaluationform#getStudent <em>Student</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Evaluation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Evaluation</em>' reference list.
	 * @see tdt4250.courses.CoursesPackage#getStudent_Evaluation()
	 * @see tdt4250.courses.Evaluationform#getStudent
	 * @model opposite="student"
	 * @generated
	 */
	EList<Evaluationform> getEvaluation();

} // Student
