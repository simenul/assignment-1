/**
 */
package tdt4250.courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Timetable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.Timetable#getCourseinstance <em>Courseinstance</em>}</li>
 *   <li>{@link tdt4250.courses.Timetable#getTtentry <em>Ttentry</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getTimetable()
 * @model
 * @generated
 */
public interface Timetable extends EObject {
	/**
	 * Returns the value of the '<em><b>Courseinstance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.courses.Courseinstance#getTimetable <em>Timetable</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Courseinstance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courseinstance</em>' container reference.
	 * @see #setCourseinstance(Courseinstance)
	 * @see tdt4250.courses.CoursesPackage#getTimetable_Courseinstance()
	 * @see tdt4250.courses.Courseinstance#getTimetable
	 * @model opposite="timetable" required="true" transient="false"
	 * @generated
	 */
	Courseinstance getCourseinstance();

	/**
	 * Sets the value of the '{@link tdt4250.courses.Timetable#getCourseinstance <em>Courseinstance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Courseinstance</em>' container reference.
	 * @see #getCourseinstance()
	 * @generated
	 */
	void setCourseinstance(Courseinstance value);

	/**
	 * Returns the value of the '<em><b>Ttentry</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.courses.ttEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ttentry</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ttentry</em>' containment reference list.
	 * @see tdt4250.courses.CoursesPackage#getTimetable_Ttentry()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<ttEntry> getTtentry();

} // Timetable
