/**
 */
package tdt4250.courses.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import tdt4250.courses.CourseCordinator;
import tdt4250.courses.CoursesPackage;
import tdt4250.courses.Organisation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course Cordinator</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.impl.CourseCordinatorImpl#getOrganisitation <em>Organisitation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseCordinatorImpl extends PersonImpl implements CourseCordinator {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseCordinatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.COURSE_CORDINATOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Organisation getOrganisitation() {
		if (eContainerFeatureID() != CoursesPackage.COURSE_CORDINATOR__ORGANISITATION)
			return null;
		return (Organisation) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrganisitation(Organisation newOrganisitation, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newOrganisitation, CoursesPackage.COURSE_CORDINATOR__ORGANISITATION,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrganisitation(Organisation newOrganisitation) {
		if (newOrganisitation != eInternalContainer()
				|| (eContainerFeatureID() != CoursesPackage.COURSE_CORDINATOR__ORGANISITATION
						&& newOrganisitation != null)) {
			if (EcoreUtil.isAncestor(this, newOrganisitation))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newOrganisitation != null)
				msgs = ((InternalEObject) newOrganisitation).eInverseAdd(this,
						CoursesPackage.ORGANISATION__COURSECORDINATOR, Organisation.class, msgs);
			msgs = basicSetOrganisitation(newOrganisitation, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE_CORDINATOR__ORGANISITATION,
					newOrganisitation, newOrganisitation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.COURSE_CORDINATOR__ORGANISITATION:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetOrganisitation((Organisation) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.COURSE_CORDINATOR__ORGANISITATION:
			return basicSetOrganisitation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case CoursesPackage.COURSE_CORDINATOR__ORGANISITATION:
			return eInternalContainer().eInverseRemove(this, CoursesPackage.ORGANISATION__COURSECORDINATOR,
					Organisation.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursesPackage.COURSE_CORDINATOR__ORGANISITATION:
			return getOrganisitation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursesPackage.COURSE_CORDINATOR__ORGANISITATION:
			setOrganisitation((Organisation) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursesPackage.COURSE_CORDINATOR__ORGANISITATION:
			setOrganisitation((Organisation) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursesPackage.COURSE_CORDINATOR__ORGANISITATION:
			return getOrganisitation() != null;
		}
		return super.eIsSet(featureID);
	}

} //CourseCordinatorImpl
