/**
 */
package tdt4250.courses.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.courses.Course;
import tdt4250.courses.Courseinstance;
import tdt4250.courses.CoursesPackage;
import tdt4250.courses.Department;
import tdt4250.courses.Studyprogram;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.impl.CourseImpl#getCourseCode <em>Course Code</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseImpl#getName <em>Name</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseImpl#getContent <em>Content</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseImpl#getCredits <em>Credits</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseImpl#getCourseInstances <em>Course Instances</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseImpl#getDepartment <em>Department</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseImpl#getRequiredcondition <em>Requiredcondition</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseImpl#getRecommendedcondition <em>Recommendedcondition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseImpl extends MinimalEObjectImpl.Container implements Course {
	/**
	 * The default value of the '{@link #getCourseCode() <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCode()
	 * @generated
	 * @ordered
	 */
	protected static final String COURSE_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCourseCode() <em>Course Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseCode()
	 * @generated
	 * @ordered
	 */
	protected String courseCode = COURSE_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected static final String CONTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getContent() <em>Content</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContent()
	 * @generated
	 * @ordered
	 */
	protected String content = CONTENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected static final double CREDITS_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCredits() <em>Credits</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCredits()
	 * @generated
	 * @ordered
	 */
	protected double credits = CREDITS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourseInstances() <em>Course Instances</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourseInstances()
	 * @generated
	 * @ordered
	 */
	protected EList<Courseinstance> courseInstances;

	/**
	 * The cached value of the '{@link #getRequiredcondition() <em>Requiredcondition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredcondition()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> requiredcondition;

	/**
	 * The cached value of the '{@link #getRecommendedcondition() <em>Recommendedcondition</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecommendedcondition()
	 * @generated
	 * @ordered
	 */
	protected EList<Studyprogram> recommendedcondition;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.COURSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCourseCode() {
		return courseCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseCode(String newCourseCode) {
		String oldCourseCode = courseCode;
		courseCode = newCourseCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__COURSE_CODE, oldCourseCode,
					courseCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getContent() {
		return content;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContent(String newContent) {
		String oldContent = content;
		content = newContent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__CONTENT, oldContent, content));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getCredits() {
		return credits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCredits(double newCredits) {
		double oldCredits = credits;
		credits = newCredits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__CREDITS, oldCredits, credits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Courseinstance> getCourseInstances() {
		if (courseInstances == null) {
			courseInstances = new EObjectContainmentWithInverseEList<Courseinstance>(Courseinstance.class, this,
					CoursesPackage.COURSE__COURSE_INSTANCES, CoursesPackage.COURSEINSTANCE__COURSE);
		}
		return courseInstances;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department getDepartment() {
		if (eContainerFeatureID() != CoursesPackage.COURSE__DEPARTMENT)
			return null;
		return (Department) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDepartment(Department newDepartment, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newDepartment, CoursesPackage.COURSE__DEPARTMENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepartment(Department newDepartment) {
		if (newDepartment != eInternalContainer()
				|| (eContainerFeatureID() != CoursesPackage.COURSE__DEPARTMENT && newDepartment != null)) {
			if (EcoreUtil.isAncestor(this, newDepartment))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newDepartment != null)
				msgs = ((InternalEObject) newDepartment).eInverseAdd(this, CoursesPackage.DEPARTMENT__COURSES,
						Department.class, msgs);
			msgs = basicSetDepartment(newDepartment, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSE__DEPARTMENT, newDepartment,
					newDepartment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getRequiredcondition() {
		if (requiredcondition == null) {
			requiredcondition = new EObjectResolvingEList<Course>(Course.class, this,
					CoursesPackage.COURSE__REQUIREDCONDITION);
		}
		return requiredcondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Studyprogram> getRecommendedcondition() {
		if (recommendedcondition == null) {
			recommendedcondition = new EObjectResolvingEList<Studyprogram>(Studyprogram.class, this,
					CoursesPackage.COURSE__RECOMMENDEDCONDITION);
		}
		return recommendedcondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.COURSE__COURSE_INSTANCES:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCourseInstances()).basicAdd(otherEnd, msgs);
		case CoursesPackage.COURSE__DEPARTMENT:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetDepartment((Department) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.COURSE__COURSE_INSTANCES:
			return ((InternalEList<?>) getCourseInstances()).basicRemove(otherEnd, msgs);
		case CoursesPackage.COURSE__DEPARTMENT:
			return basicSetDepartment(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case CoursesPackage.COURSE__DEPARTMENT:
			return eInternalContainer().eInverseRemove(this, CoursesPackage.DEPARTMENT__COURSES, Department.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursesPackage.COURSE__COURSE_CODE:
			return getCourseCode();
		case CoursesPackage.COURSE__NAME:
			return getName();
		case CoursesPackage.COURSE__CONTENT:
			return getContent();
		case CoursesPackage.COURSE__CREDITS:
			return getCredits();
		case CoursesPackage.COURSE__COURSE_INSTANCES:
			return getCourseInstances();
		case CoursesPackage.COURSE__DEPARTMENT:
			return getDepartment();
		case CoursesPackage.COURSE__REQUIREDCONDITION:
			return getRequiredcondition();
		case CoursesPackage.COURSE__RECOMMENDEDCONDITION:
			return getRecommendedcondition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursesPackage.COURSE__COURSE_CODE:
			setCourseCode((String) newValue);
			return;
		case CoursesPackage.COURSE__NAME:
			setName((String) newValue);
			return;
		case CoursesPackage.COURSE__CONTENT:
			setContent((String) newValue);
			return;
		case CoursesPackage.COURSE__CREDITS:
			setCredits((Double) newValue);
			return;
		case CoursesPackage.COURSE__COURSE_INSTANCES:
			getCourseInstances().clear();
			getCourseInstances().addAll((Collection<? extends Courseinstance>) newValue);
			return;
		case CoursesPackage.COURSE__DEPARTMENT:
			setDepartment((Department) newValue);
			return;
		case CoursesPackage.COURSE__REQUIREDCONDITION:
			getRequiredcondition().clear();
			getRequiredcondition().addAll((Collection<? extends Course>) newValue);
			return;
		case CoursesPackage.COURSE__RECOMMENDEDCONDITION:
			getRecommendedcondition().clear();
			getRecommendedcondition().addAll((Collection<? extends Studyprogram>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursesPackage.COURSE__COURSE_CODE:
			setCourseCode(COURSE_CODE_EDEFAULT);
			return;
		case CoursesPackage.COURSE__NAME:
			setName(NAME_EDEFAULT);
			return;
		case CoursesPackage.COURSE__CONTENT:
			setContent(CONTENT_EDEFAULT);
			return;
		case CoursesPackage.COURSE__CREDITS:
			setCredits(CREDITS_EDEFAULT);
			return;
		case CoursesPackage.COURSE__COURSE_INSTANCES:
			getCourseInstances().clear();
			return;
		case CoursesPackage.COURSE__DEPARTMENT:
			setDepartment((Department) null);
			return;
		case CoursesPackage.COURSE__REQUIREDCONDITION:
			getRequiredcondition().clear();
			return;
		case CoursesPackage.COURSE__RECOMMENDEDCONDITION:
			getRecommendedcondition().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursesPackage.COURSE__COURSE_CODE:
			return COURSE_CODE_EDEFAULT == null ? courseCode != null : !COURSE_CODE_EDEFAULT.equals(courseCode);
		case CoursesPackage.COURSE__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case CoursesPackage.COURSE__CONTENT:
			return CONTENT_EDEFAULT == null ? content != null : !CONTENT_EDEFAULT.equals(content);
		case CoursesPackage.COURSE__CREDITS:
			return credits != CREDITS_EDEFAULT;
		case CoursesPackage.COURSE__COURSE_INSTANCES:
			return courseInstances != null && !courseInstances.isEmpty();
		case CoursesPackage.COURSE__DEPARTMENT:
			return getDepartment() != null;
		case CoursesPackage.COURSE__REQUIREDCONDITION:
			return requiredcondition != null && !requiredcondition.isEmpty();
		case CoursesPackage.COURSE__RECOMMENDEDCONDITION:
			return recommendedcondition != null && !recommendedcondition.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (courseCode: ");
		result.append(courseCode);
		result.append(", name: ");
		result.append(name);
		result.append(", content: ");
		result.append(content);
		result.append(", credits: ");
		result.append(credits);
		result.append(')');
		return result.toString();
	}

} //CourseImpl
