/**
 */
package tdt4250.courses.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import tdt4250.courses.Course;
import tdt4250.courses.Courseinstance;
import tdt4250.courses.CoursesPackage;
import tdt4250.courses.Coursework;
import tdt4250.courses.Evaluationform;
import tdt4250.courses.Organisation;
import tdt4250.courses.Timetable;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Courseinstance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.impl.CourseinstanceImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseinstanceImpl#getOrganisation <em>Organisation</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseinstanceImpl#getEvaluationform <em>Evaluationform</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseinstanceImpl#getCoursework <em>Coursework</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseinstanceImpl#getTimetable <em>Timetable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseinstanceImpl extends MinimalEObjectImpl.Container implements Courseinstance {
	/**
	 * The cached value of the '{@link #getOrganisation() <em>Organisation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrganisation()
	 * @generated
	 * @ordered
	 */
	protected Organisation organisation;

	/**
	 * The cached value of the '{@link #getEvaluationform() <em>Evaluationform</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluationform()
	 * @generated
	 * @ordered
	 */
	protected Evaluationform evaluationform;

	/**
	 * The cached value of the '{@link #getCoursework() <em>Coursework</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoursework()
	 * @generated
	 * @ordered
	 */
	protected Coursework coursework;

	/**
	 * The cached value of the '{@link #getTimetable() <em>Timetable</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTimetable()
	 * @generated
	 * @ordered
	 */
	protected Timetable timetable;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseinstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.COURSEINSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course getCourse() {
		if (eContainerFeatureID() != CoursesPackage.COURSEINSTANCE__COURSE)
			return null;
		return (Course) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourse(Course newCourse, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourse, CoursesPackage.COURSEINSTANCE__COURSE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourse(Course newCourse) {
		if (newCourse != eInternalContainer()
				|| (eContainerFeatureID() != CoursesPackage.COURSEINSTANCE__COURSE && newCourse != null)) {
			if (EcoreUtil.isAncestor(this, newCourse))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourse != null)
				msgs = ((InternalEObject) newCourse).eInverseAdd(this, CoursesPackage.COURSE__COURSE_INSTANCES,
						Course.class, msgs);
			msgs = basicSetCourse(newCourse, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSEINSTANCE__COURSE, newCourse,
					newCourse));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Organisation getOrganisation() {
		return organisation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrganisation(Organisation newOrganisation, NotificationChain msgs) {
		Organisation oldOrganisation = organisation;
		organisation = newOrganisation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					CoursesPackage.COURSEINSTANCE__ORGANISATION, oldOrganisation, newOrganisation);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrganisation(Organisation newOrganisation) {
		if (newOrganisation != organisation) {
			NotificationChain msgs = null;
			if (organisation != null)
				msgs = ((InternalEObject) organisation).eInverseRemove(this,
						CoursesPackage.ORGANISATION__COURSEINSTANCE, Organisation.class, msgs);
			if (newOrganisation != null)
				msgs = ((InternalEObject) newOrganisation).eInverseAdd(this,
						CoursesPackage.ORGANISATION__COURSEINSTANCE, Organisation.class, msgs);
			msgs = basicSetOrganisation(newOrganisation, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSEINSTANCE__ORGANISATION,
					newOrganisation, newOrganisation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Evaluationform getEvaluationform() {
		return evaluationform;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEvaluationform(Evaluationform newEvaluationform, NotificationChain msgs) {
		Evaluationform oldEvaluationform = evaluationform;
		evaluationform = newEvaluationform;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					CoursesPackage.COURSEINSTANCE__EVALUATIONFORM, oldEvaluationform, newEvaluationform);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvaluationform(Evaluationform newEvaluationform) {
		if (newEvaluationform != evaluationform) {
			NotificationChain msgs = null;
			if (evaluationform != null)
				msgs = ((InternalEObject) evaluationform).eInverseRemove(this,
						CoursesPackage.EVALUATIONFORM__COURSEINSTANCE, Evaluationform.class, msgs);
			if (newEvaluationform != null)
				msgs = ((InternalEObject) newEvaluationform).eInverseAdd(this,
						CoursesPackage.EVALUATIONFORM__COURSEINSTANCE, Evaluationform.class, msgs);
			msgs = basicSetEvaluationform(newEvaluationform, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSEINSTANCE__EVALUATIONFORM,
					newEvaluationform, newEvaluationform));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Coursework getCoursework() {
		return coursework;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCoursework(Coursework newCoursework, NotificationChain msgs) {
		Coursework oldCoursework = coursework;
		coursework = newCoursework;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					CoursesPackage.COURSEINSTANCE__COURSEWORK, oldCoursework, newCoursework);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoursework(Coursework newCoursework) {
		if (newCoursework != coursework) {
			NotificationChain msgs = null;
			if (coursework != null)
				msgs = ((InternalEObject) coursework).eInverseRemove(this, CoursesPackage.COURSEWORK__COURSEINSTANCE,
						Coursework.class, msgs);
			if (newCoursework != null)
				msgs = ((InternalEObject) newCoursework).eInverseAdd(this, CoursesPackage.COURSEWORK__COURSEINSTANCE,
						Coursework.class, msgs);
			msgs = basicSetCoursework(newCoursework, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSEINSTANCE__COURSEWORK,
					newCoursework, newCoursework));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timetable getTimetable() {
		return timetable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTimetable(Timetable newTimetable, NotificationChain msgs) {
		Timetable oldTimetable = timetable;
		timetable = newTimetable;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					CoursesPackage.COURSEINSTANCE__TIMETABLE, oldTimetable, newTimetable);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimetable(Timetable newTimetable) {
		if (newTimetable != timetable) {
			NotificationChain msgs = null;
			if (timetable != null)
				msgs = ((InternalEObject) timetable).eInverseRemove(this, CoursesPackage.TIMETABLE__COURSEINSTANCE,
						Timetable.class, msgs);
			if (newTimetable != null)
				msgs = ((InternalEObject) newTimetable).eInverseAdd(this, CoursesPackage.TIMETABLE__COURSEINSTANCE,
						Timetable.class, msgs);
			msgs = basicSetTimetable(newTimetable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSEINSTANCE__TIMETABLE,
					newTimetable, newTimetable));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.COURSEINSTANCE__COURSE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourse((Course) otherEnd, msgs);
		case CoursesPackage.COURSEINSTANCE__ORGANISATION:
			if (organisation != null)
				msgs = ((InternalEObject) organisation).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - CoursesPackage.COURSEINSTANCE__ORGANISATION, null, msgs);
			return basicSetOrganisation((Organisation) otherEnd, msgs);
		case CoursesPackage.COURSEINSTANCE__EVALUATIONFORM:
			if (evaluationform != null)
				msgs = ((InternalEObject) evaluationform).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - CoursesPackage.COURSEINSTANCE__EVALUATIONFORM, null, msgs);
			return basicSetEvaluationform((Evaluationform) otherEnd, msgs);
		case CoursesPackage.COURSEINSTANCE__COURSEWORK:
			if (coursework != null)
				msgs = ((InternalEObject) coursework).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - CoursesPackage.COURSEINSTANCE__COURSEWORK, null, msgs);
			return basicSetCoursework((Coursework) otherEnd, msgs);
		case CoursesPackage.COURSEINSTANCE__TIMETABLE:
			if (timetable != null)
				msgs = ((InternalEObject) timetable).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - CoursesPackage.COURSEINSTANCE__TIMETABLE, null, msgs);
			return basicSetTimetable((Timetable) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.COURSEINSTANCE__COURSE:
			return basicSetCourse(null, msgs);
		case CoursesPackage.COURSEINSTANCE__ORGANISATION:
			return basicSetOrganisation(null, msgs);
		case CoursesPackage.COURSEINSTANCE__EVALUATIONFORM:
			return basicSetEvaluationform(null, msgs);
		case CoursesPackage.COURSEINSTANCE__COURSEWORK:
			return basicSetCoursework(null, msgs);
		case CoursesPackage.COURSEINSTANCE__TIMETABLE:
			return basicSetTimetable(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case CoursesPackage.COURSEINSTANCE__COURSE:
			return eInternalContainer().eInverseRemove(this, CoursesPackage.COURSE__COURSE_INSTANCES, Course.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursesPackage.COURSEINSTANCE__COURSE:
			return getCourse();
		case CoursesPackage.COURSEINSTANCE__ORGANISATION:
			return getOrganisation();
		case CoursesPackage.COURSEINSTANCE__EVALUATIONFORM:
			return getEvaluationform();
		case CoursesPackage.COURSEINSTANCE__COURSEWORK:
			return getCoursework();
		case CoursesPackage.COURSEINSTANCE__TIMETABLE:
			return getTimetable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursesPackage.COURSEINSTANCE__COURSE:
			setCourse((Course) newValue);
			return;
		case CoursesPackage.COURSEINSTANCE__ORGANISATION:
			setOrganisation((Organisation) newValue);
			return;
		case CoursesPackage.COURSEINSTANCE__EVALUATIONFORM:
			setEvaluationform((Evaluationform) newValue);
			return;
		case CoursesPackage.COURSEINSTANCE__COURSEWORK:
			setCoursework((Coursework) newValue);
			return;
		case CoursesPackage.COURSEINSTANCE__TIMETABLE:
			setTimetable((Timetable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursesPackage.COURSEINSTANCE__COURSE:
			setCourse((Course) null);
			return;
		case CoursesPackage.COURSEINSTANCE__ORGANISATION:
			setOrganisation((Organisation) null);
			return;
		case CoursesPackage.COURSEINSTANCE__EVALUATIONFORM:
			setEvaluationform((Evaluationform) null);
			return;
		case CoursesPackage.COURSEINSTANCE__COURSEWORK:
			setCoursework((Coursework) null);
			return;
		case CoursesPackage.COURSEINSTANCE__TIMETABLE:
			setTimetable((Timetable) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursesPackage.COURSEINSTANCE__COURSE:
			return getCourse() != null;
		case CoursesPackage.COURSEINSTANCE__ORGANISATION:
			return organisation != null;
		case CoursesPackage.COURSEINSTANCE__EVALUATIONFORM:
			return evaluationform != null;
		case CoursesPackage.COURSEINSTANCE__COURSEWORK:
			return coursework != null;
		case CoursesPackage.COURSEINSTANCE__TIMETABLE:
			return timetable != null;
		}
		return super.eIsSet(featureID);
	}

} //CourseinstanceImpl
