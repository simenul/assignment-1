/**
 */
package tdt4250.courses.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import tdt4250.courses.Courseinstance;
import tdt4250.courses.CoursesPackage;
import tdt4250.courses.Coursework;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Coursework</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.impl.CourseworkImpl#getLecturehours <em>Lecturehours</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseworkImpl#getLabhours <em>Labhours</em>}</li>
 *   <li>{@link tdt4250.courses.impl.CourseworkImpl#getCourseinstance <em>Courseinstance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CourseworkImpl extends MinimalEObjectImpl.Container implements Coursework {
	/**
	 * The default value of the '{@link #getLecturehours() <em>Lecturehours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLecturehours()
	 * @generated
	 * @ordered
	 */
	protected static final int LECTUREHOURS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLecturehours() <em>Lecturehours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLecturehours()
	 * @generated
	 * @ordered
	 */
	protected int lecturehours = LECTUREHOURS_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabhours() <em>Labhours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabhours()
	 * @generated
	 * @ordered
	 */
	protected static final int LABHOURS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLabhours() <em>Labhours</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabhours()
	 * @generated
	 * @ordered
	 */
	protected int labhours = LABHOURS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CourseworkImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.COURSEWORK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLecturehours() {
		return lecturehours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLecturehours(int newLecturehours) {
		int oldLecturehours = lecturehours;
		lecturehours = newLecturehours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSEWORK__LECTUREHOURS,
					oldLecturehours, lecturehours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLabhours() {
		return labhours;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabhours(int newLabhours) {
		int oldLabhours = labhours;
		labhours = newLabhours;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSEWORK__LABHOURS, oldLabhours,
					labhours));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Courseinstance getCourseinstance() {
		if (eContainerFeatureID() != CoursesPackage.COURSEWORK__COURSEINSTANCE)
			return null;
		return (Courseinstance) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseinstance(Courseinstance newCourseinstance, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourseinstance, CoursesPackage.COURSEWORK__COURSEINSTANCE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseinstance(Courseinstance newCourseinstance) {
		if (newCourseinstance != eInternalContainer()
				|| (eContainerFeatureID() != CoursesPackage.COURSEWORK__COURSEINSTANCE && newCourseinstance != null)) {
			if (EcoreUtil.isAncestor(this, newCourseinstance))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourseinstance != null)
				msgs = ((InternalEObject) newCourseinstance).eInverseAdd(this,
						CoursesPackage.COURSEINSTANCE__COURSEWORK, Courseinstance.class, msgs);
			msgs = basicSetCourseinstance(newCourseinstance, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.COURSEWORK__COURSEINSTANCE,
					newCourseinstance, newCourseinstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.COURSEWORK__COURSEINSTANCE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourseinstance((Courseinstance) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.COURSEWORK__COURSEINSTANCE:
			return basicSetCourseinstance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case CoursesPackage.COURSEWORK__COURSEINSTANCE:
			return eInternalContainer().eInverseRemove(this, CoursesPackage.COURSEINSTANCE__COURSEWORK,
					Courseinstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursesPackage.COURSEWORK__LECTUREHOURS:
			return getLecturehours();
		case CoursesPackage.COURSEWORK__LABHOURS:
			return getLabhours();
		case CoursesPackage.COURSEWORK__COURSEINSTANCE:
			return getCourseinstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursesPackage.COURSEWORK__LECTUREHOURS:
			setLecturehours((Integer) newValue);
			return;
		case CoursesPackage.COURSEWORK__LABHOURS:
			setLabhours((Integer) newValue);
			return;
		case CoursesPackage.COURSEWORK__COURSEINSTANCE:
			setCourseinstance((Courseinstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursesPackage.COURSEWORK__LECTUREHOURS:
			setLecturehours(LECTUREHOURS_EDEFAULT);
			return;
		case CoursesPackage.COURSEWORK__LABHOURS:
			setLabhours(LABHOURS_EDEFAULT);
			return;
		case CoursesPackage.COURSEWORK__COURSEINSTANCE:
			setCourseinstance((Courseinstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursesPackage.COURSEWORK__LECTUREHOURS:
			return lecturehours != LECTUREHOURS_EDEFAULT;
		case CoursesPackage.COURSEWORK__LABHOURS:
			return labhours != LABHOURS_EDEFAULT;
		case CoursesPackage.COURSEWORK__COURSEINSTANCE:
			return getCourseinstance() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (lecturehours: ");
		result.append(lecturehours);
		result.append(", labhours: ");
		result.append(labhours);
		result.append(')');
		return result.toString();
	}

} //CourseworkImpl
