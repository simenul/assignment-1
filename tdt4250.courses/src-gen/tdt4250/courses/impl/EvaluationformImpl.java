/**
 */
package tdt4250.courses.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.courses.Courseinstance;
import tdt4250.courses.CoursesPackage;
import tdt4250.courses.Evaluationform;
import tdt4250.courses.Student;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Evaluationform</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.impl.EvaluationformImpl#getExam <em>Exam</em>}</li>
 *   <li>{@link tdt4250.courses.impl.EvaluationformImpl#getAssignments <em>Assignments</em>}</li>
 *   <li>{@link tdt4250.courses.impl.EvaluationformImpl#getProject <em>Project</em>}</li>
 *   <li>{@link tdt4250.courses.impl.EvaluationformImpl#getCourseinstance <em>Courseinstance</em>}</li>
 *   <li>{@link tdt4250.courses.impl.EvaluationformImpl#getStudent <em>Student</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EvaluationformImpl extends MinimalEObjectImpl.Container implements Evaluationform {
	/**
	 * The default value of the '{@link #getExam() <em>Exam</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExam()
	 * @generated
	 * @ordered
	 */
	protected static final int EXAM_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getExam() <em>Exam</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExam()
	 * @generated
	 * @ordered
	 */
	protected int exam = EXAM_EDEFAULT;

	/**
	 * The default value of the '{@link #getAssignments() <em>Assignments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignments()
	 * @generated
	 * @ordered
	 */
	protected static final int ASSIGNMENTS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getAssignments() <em>Assignments</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAssignments()
	 * @generated
	 * @ordered
	 */
	protected int assignments = ASSIGNMENTS_EDEFAULT;

	/**
	 * The default value of the '{@link #getProject() <em>Project</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProject()
	 * @generated
	 * @ordered
	 */
	protected static final int PROJECT_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getProject() <em>Project</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProject()
	 * @generated
	 * @ordered
	 */
	protected int project = PROJECT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStudent() <em>Student</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudent()
	 * @generated
	 * @ordered
	 */
	protected EList<Student> student;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EvaluationformImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.EVALUATIONFORM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getExam() {
		return exam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExam(int newExam) {
		int oldExam = exam;
		exam = newExam;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.EVALUATIONFORM__EXAM, oldExam, exam));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getAssignments() {
		return assignments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssignments(int newAssignments) {
		int oldAssignments = assignments;
		assignments = newAssignments;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.EVALUATIONFORM__ASSIGNMENTS,
					oldAssignments, assignments));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getProject() {
		return project;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProject(int newProject) {
		int oldProject = project;
		project = newProject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.EVALUATIONFORM__PROJECT, oldProject,
					project));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Courseinstance getCourseinstance() {
		if (eContainerFeatureID() != CoursesPackage.EVALUATIONFORM__COURSEINSTANCE)
			return null;
		return (Courseinstance) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseinstance(Courseinstance newCourseinstance, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourseinstance, CoursesPackage.EVALUATIONFORM__COURSEINSTANCE,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseinstance(Courseinstance newCourseinstance) {
		if (newCourseinstance != eInternalContainer()
				|| (eContainerFeatureID() != CoursesPackage.EVALUATIONFORM__COURSEINSTANCE
						&& newCourseinstance != null)) {
			if (EcoreUtil.isAncestor(this, newCourseinstance))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourseinstance != null)
				msgs = ((InternalEObject) newCourseinstance).eInverseAdd(this,
						CoursesPackage.COURSEINSTANCE__EVALUATIONFORM, Courseinstance.class, msgs);
			msgs = basicSetCourseinstance(newCourseinstance, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.EVALUATIONFORM__COURSEINSTANCE,
					newCourseinstance, newCourseinstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Student> getStudent() {
		if (student == null) {
			student = new EObjectWithInverseResolvingEList.ManyInverse<Student>(Student.class, this,
					CoursesPackage.EVALUATIONFORM__STUDENT, CoursesPackage.STUDENT__EVALUATION);
		}
		return student;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.EVALUATIONFORM__COURSEINSTANCE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourseinstance((Courseinstance) otherEnd, msgs);
		case CoursesPackage.EVALUATIONFORM__STUDENT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getStudent()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.EVALUATIONFORM__COURSEINSTANCE:
			return basicSetCourseinstance(null, msgs);
		case CoursesPackage.EVALUATIONFORM__STUDENT:
			return ((InternalEList<?>) getStudent()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case CoursesPackage.EVALUATIONFORM__COURSEINSTANCE:
			return eInternalContainer().eInverseRemove(this, CoursesPackage.COURSEINSTANCE__EVALUATIONFORM,
					Courseinstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursesPackage.EVALUATIONFORM__EXAM:
			return getExam();
		case CoursesPackage.EVALUATIONFORM__ASSIGNMENTS:
			return getAssignments();
		case CoursesPackage.EVALUATIONFORM__PROJECT:
			return getProject();
		case CoursesPackage.EVALUATIONFORM__COURSEINSTANCE:
			return getCourseinstance();
		case CoursesPackage.EVALUATIONFORM__STUDENT:
			return getStudent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursesPackage.EVALUATIONFORM__EXAM:
			setExam((Integer) newValue);
			return;
		case CoursesPackage.EVALUATIONFORM__ASSIGNMENTS:
			setAssignments((Integer) newValue);
			return;
		case CoursesPackage.EVALUATIONFORM__PROJECT:
			setProject((Integer) newValue);
			return;
		case CoursesPackage.EVALUATIONFORM__COURSEINSTANCE:
			setCourseinstance((Courseinstance) newValue);
			return;
		case CoursesPackage.EVALUATIONFORM__STUDENT:
			getStudent().clear();
			getStudent().addAll((Collection<? extends Student>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursesPackage.EVALUATIONFORM__EXAM:
			setExam(EXAM_EDEFAULT);
			return;
		case CoursesPackage.EVALUATIONFORM__ASSIGNMENTS:
			setAssignments(ASSIGNMENTS_EDEFAULT);
			return;
		case CoursesPackage.EVALUATIONFORM__PROJECT:
			setProject(PROJECT_EDEFAULT);
			return;
		case CoursesPackage.EVALUATIONFORM__COURSEINSTANCE:
			setCourseinstance((Courseinstance) null);
			return;
		case CoursesPackage.EVALUATIONFORM__STUDENT:
			getStudent().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursesPackage.EVALUATIONFORM__EXAM:
			return exam != EXAM_EDEFAULT;
		case CoursesPackage.EVALUATIONFORM__ASSIGNMENTS:
			return assignments != ASSIGNMENTS_EDEFAULT;
		case CoursesPackage.EVALUATIONFORM__PROJECT:
			return project != PROJECT_EDEFAULT;
		case CoursesPackage.EVALUATIONFORM__COURSEINSTANCE:
			return getCourseinstance() != null;
		case CoursesPackage.EVALUATIONFORM__STUDENT:
			return student != null && !student.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (exam: ");
		result.append(exam);
		result.append(", assignments: ");
		result.append(assignments);
		result.append(", project: ");
		result.append(project);
		result.append(')');
		return result.toString();
	}

} //EvaluationformImpl
