/**
 */
package tdt4250.courses.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.courses.CourseCordinator;
import tdt4250.courses.Courseinstance;
import tdt4250.courses.CoursesPackage;
import tdt4250.courses.Lecturer;
import tdt4250.courses.Organisation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Organisation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.impl.OrganisationImpl#getCoursecordinator <em>Coursecordinator</em>}</li>
 *   <li>{@link tdt4250.courses.impl.OrganisationImpl#getLecturer <em>Lecturer</em>}</li>
 *   <li>{@link tdt4250.courses.impl.OrganisationImpl#getCourseinstance <em>Courseinstance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OrganisationImpl extends MinimalEObjectImpl.Container implements Organisation {
	/**
	 * The cached value of the '{@link #getCoursecordinator() <em>Coursecordinator</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCoursecordinator()
	 * @generated
	 * @ordered
	 */
	protected CourseCordinator coursecordinator;

	/**
	 * The cached value of the '{@link #getLecturer() <em>Lecturer</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLecturer()
	 * @generated
	 * @ordered
	 */
	protected EList<Lecturer> lecturer;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrganisationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.ORGANISATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseCordinator getCoursecordinator() {
		return coursecordinator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCoursecordinator(CourseCordinator newCoursecordinator, NotificationChain msgs) {
		CourseCordinator oldCoursecordinator = coursecordinator;
		coursecordinator = newCoursecordinator;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					CoursesPackage.ORGANISATION__COURSECORDINATOR, oldCoursecordinator, newCoursecordinator);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoursecordinator(CourseCordinator newCoursecordinator) {
		if (newCoursecordinator != coursecordinator) {
			NotificationChain msgs = null;
			if (coursecordinator != null)
				msgs = ((InternalEObject) coursecordinator).eInverseRemove(this,
						CoursesPackage.COURSE_CORDINATOR__ORGANISITATION, CourseCordinator.class, msgs);
			if (newCoursecordinator != null)
				msgs = ((InternalEObject) newCoursecordinator).eInverseAdd(this,
						CoursesPackage.COURSE_CORDINATOR__ORGANISITATION, CourseCordinator.class, msgs);
			msgs = basicSetCoursecordinator(newCoursecordinator, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.ORGANISATION__COURSECORDINATOR,
					newCoursecordinator, newCoursecordinator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Lecturer> getLecturer() {
		if (lecturer == null) {
			lecturer = new EObjectContainmentWithInverseEList<Lecturer>(Lecturer.class, this,
					CoursesPackage.ORGANISATION__LECTURER, CoursesPackage.LECTURER__ORGANISATION);
		}
		return lecturer;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Courseinstance getCourseinstance() {
		if (eContainerFeatureID() != CoursesPackage.ORGANISATION__COURSEINSTANCE)
			return null;
		return (Courseinstance) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCourseinstance(Courseinstance newCourseinstance, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCourseinstance, CoursesPackage.ORGANISATION__COURSEINSTANCE,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCourseinstance(Courseinstance newCourseinstance) {
		if (newCourseinstance != eInternalContainer()
				|| (eContainerFeatureID() != CoursesPackage.ORGANISATION__COURSEINSTANCE
						&& newCourseinstance != null)) {
			if (EcoreUtil.isAncestor(this, newCourseinstance))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCourseinstance != null)
				msgs = ((InternalEObject) newCourseinstance).eInverseAdd(this,
						CoursesPackage.COURSEINSTANCE__ORGANISATION, Courseinstance.class, msgs);
			msgs = basicSetCourseinstance(newCourseinstance, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.ORGANISATION__COURSEINSTANCE,
					newCourseinstance, newCourseinstance));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.ORGANISATION__COURSECORDINATOR:
			if (coursecordinator != null)
				msgs = ((InternalEObject) coursecordinator).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - CoursesPackage.ORGANISATION__COURSECORDINATOR, null, msgs);
			return basicSetCoursecordinator((CourseCordinator) otherEnd, msgs);
		case CoursesPackage.ORGANISATION__LECTURER:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getLecturer()).basicAdd(otherEnd, msgs);
		case CoursesPackage.ORGANISATION__COURSEINSTANCE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCourseinstance((Courseinstance) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.ORGANISATION__COURSECORDINATOR:
			return basicSetCoursecordinator(null, msgs);
		case CoursesPackage.ORGANISATION__LECTURER:
			return ((InternalEList<?>) getLecturer()).basicRemove(otherEnd, msgs);
		case CoursesPackage.ORGANISATION__COURSEINSTANCE:
			return basicSetCourseinstance(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case CoursesPackage.ORGANISATION__COURSEINSTANCE:
			return eInternalContainer().eInverseRemove(this, CoursesPackage.COURSEINSTANCE__ORGANISATION,
					Courseinstance.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursesPackage.ORGANISATION__COURSECORDINATOR:
			return getCoursecordinator();
		case CoursesPackage.ORGANISATION__LECTURER:
			return getLecturer();
		case CoursesPackage.ORGANISATION__COURSEINSTANCE:
			return getCourseinstance();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursesPackage.ORGANISATION__COURSECORDINATOR:
			setCoursecordinator((CourseCordinator) newValue);
			return;
		case CoursesPackage.ORGANISATION__LECTURER:
			getLecturer().clear();
			getLecturer().addAll((Collection<? extends Lecturer>) newValue);
			return;
		case CoursesPackage.ORGANISATION__COURSEINSTANCE:
			setCourseinstance((Courseinstance) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursesPackage.ORGANISATION__COURSECORDINATOR:
			setCoursecordinator((CourseCordinator) null);
			return;
		case CoursesPackage.ORGANISATION__LECTURER:
			getLecturer().clear();
			return;
		case CoursesPackage.ORGANISATION__COURSEINSTANCE:
			setCourseinstance((Courseinstance) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursesPackage.ORGANISATION__COURSECORDINATOR:
			return coursecordinator != null;
		case CoursesPackage.ORGANISATION__LECTURER:
			return lecturer != null && !lecturer.isEmpty();
		case CoursesPackage.ORGANISATION__COURSEINSTANCE:
			return getCourseinstance() != null;
		}
		return super.eIsSet(featureID);
	}

} //OrganisationImpl
