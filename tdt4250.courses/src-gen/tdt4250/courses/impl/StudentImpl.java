/**
 */
package tdt4250.courses.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.courses.CoursesPackage;
import tdt4250.courses.Evaluationform;
import tdt4250.courses.Student;
import tdt4250.courses.Studyprogram;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Student</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.impl.StudentImpl#getStudyprogram <em>Studyprogram</em>}</li>
 *   <li>{@link tdt4250.courses.impl.StudentImpl#getEvaluation <em>Evaluation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudentImpl extends PersonImpl implements Student {
	/**
	 * The cached value of the '{@link #getEvaluation() <em>Evaluation</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEvaluation()
	 * @generated
	 * @ordered
	 */
	protected EList<Evaluationform> evaluation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.STUDENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Studyprogram getStudyprogram() {
		if (eContainerFeatureID() != CoursesPackage.STUDENT__STUDYPROGRAM)
			return null;
		return (Studyprogram) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStudyprogram(Studyprogram newStudyprogram, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newStudyprogram, CoursesPackage.STUDENT__STUDYPROGRAM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStudyprogram(Studyprogram newStudyprogram) {
		if (newStudyprogram != eInternalContainer()
				|| (eContainerFeatureID() != CoursesPackage.STUDENT__STUDYPROGRAM && newStudyprogram != null)) {
			if (EcoreUtil.isAncestor(this, newStudyprogram))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newStudyprogram != null)
				msgs = ((InternalEObject) newStudyprogram).eInverseAdd(this, CoursesPackage.STUDYPROGRAM__STUDENTS,
						Studyprogram.class, msgs);
			msgs = basicSetStudyprogram(newStudyprogram, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.STUDENT__STUDYPROGRAM, newStudyprogram,
					newStudyprogram));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Evaluationform> getEvaluation() {
		if (evaluation == null) {
			evaluation = new EObjectWithInverseResolvingEList.ManyInverse<Evaluationform>(Evaluationform.class, this,
					CoursesPackage.STUDENT__EVALUATION, CoursesPackage.EVALUATIONFORM__STUDENT);
		}
		return evaluation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.STUDENT__STUDYPROGRAM:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetStudyprogram((Studyprogram) otherEnd, msgs);
		case CoursesPackage.STUDENT__EVALUATION:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getEvaluation()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CoursesPackage.STUDENT__STUDYPROGRAM:
			return basicSetStudyprogram(null, msgs);
		case CoursesPackage.STUDENT__EVALUATION:
			return ((InternalEList<?>) getEvaluation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case CoursesPackage.STUDENT__STUDYPROGRAM:
			return eInternalContainer().eInverseRemove(this, CoursesPackage.STUDYPROGRAM__STUDENTS, Studyprogram.class,
					msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursesPackage.STUDENT__STUDYPROGRAM:
			return getStudyprogram();
		case CoursesPackage.STUDENT__EVALUATION:
			return getEvaluation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursesPackage.STUDENT__STUDYPROGRAM:
			setStudyprogram((Studyprogram) newValue);
			return;
		case CoursesPackage.STUDENT__EVALUATION:
			getEvaluation().clear();
			getEvaluation().addAll((Collection<? extends Evaluationform>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursesPackage.STUDENT__STUDYPROGRAM:
			setStudyprogram((Studyprogram) null);
			return;
		case CoursesPackage.STUDENT__EVALUATION:
			getEvaluation().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursesPackage.STUDENT__STUDYPROGRAM:
			return getStudyprogram() != null;
		case CoursesPackage.STUDENT__EVALUATION:
			return evaluation != null && !evaluation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //StudentImpl
