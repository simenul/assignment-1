/**
 */
package tdt4250.courses.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import tdt4250.courses.CoursesPackage;
import tdt4250.courses.Day;
import tdt4250.courses.Studyprogram;
import tdt4250.courses.Type;
import tdt4250.courses.ttEntry;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>tt Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.impl.ttEntryImpl#getDay <em>Day</em>}</li>
 *   <li>{@link tdt4250.courses.impl.ttEntryImpl#getTime <em>Time</em>}</li>
 *   <li>{@link tdt4250.courses.impl.ttEntryImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link tdt4250.courses.impl.ttEntryImpl#getType <em>Type</em>}</li>
 *   <li>{@link tdt4250.courses.impl.ttEntryImpl#getStudyprogram <em>Studyprogram</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ttEntryImpl extends MinimalEObjectImpl.Container implements ttEntry {
	/**
	 * The default value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected static final Day DAY_EDEFAULT = Day.MONDAY;

	/**
	 * The cached value of the '{@link #getDay() <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDay()
	 * @generated
	 * @ordered
	 */
	protected Day day = DAY_EDEFAULT;

	/**
	 * The default value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected static final String TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTime() <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTime()
	 * @generated
	 * @ordered
	 */
	protected String time = TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoom() <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected static final String ROOM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected String room = ROOM_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final Type TYPE_EDEFAULT = Type.LECTURE;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected Type type = TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStudyprogram() <em>Studyprogram</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyprogram()
	 * @generated
	 * @ordered
	 */
	protected EList<Studyprogram> studyprogram;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ttEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CoursesPackage.Literals.TT_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Day getDay() {
		return day;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDay(Day newDay) {
		Day oldDay = day;
		day = newDay == null ? DAY_EDEFAULT : newDay;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.TT_ENTRY__DAY, oldDay, day));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTime() {
		return time;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTime(String newTime) {
		String oldTime = time;
		time = newTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.TT_ENTRY__TIME, oldTime, time));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(String newRoom) {
		String oldRoom = room;
		room = newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.TT_ENTRY__ROOM, oldRoom, room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Type getType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Type newType) {
		Type oldType = type;
		type = newType == null ? TYPE_EDEFAULT : newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CoursesPackage.TT_ENTRY__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Studyprogram> getStudyprogram() {
		if (studyprogram == null) {
			studyprogram = new EObjectResolvingEList<Studyprogram>(Studyprogram.class, this,
					CoursesPackage.TT_ENTRY__STUDYPROGRAM);
		}
		return studyprogram;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CoursesPackage.TT_ENTRY__DAY:
			return getDay();
		case CoursesPackage.TT_ENTRY__TIME:
			return getTime();
		case CoursesPackage.TT_ENTRY__ROOM:
			return getRoom();
		case CoursesPackage.TT_ENTRY__TYPE:
			return getType();
		case CoursesPackage.TT_ENTRY__STUDYPROGRAM:
			return getStudyprogram();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CoursesPackage.TT_ENTRY__DAY:
			setDay((Day) newValue);
			return;
		case CoursesPackage.TT_ENTRY__TIME:
			setTime((String) newValue);
			return;
		case CoursesPackage.TT_ENTRY__ROOM:
			setRoom((String) newValue);
			return;
		case CoursesPackage.TT_ENTRY__TYPE:
			setType((Type) newValue);
			return;
		case CoursesPackage.TT_ENTRY__STUDYPROGRAM:
			getStudyprogram().clear();
			getStudyprogram().addAll((Collection<? extends Studyprogram>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CoursesPackage.TT_ENTRY__DAY:
			setDay(DAY_EDEFAULT);
			return;
		case CoursesPackage.TT_ENTRY__TIME:
			setTime(TIME_EDEFAULT);
			return;
		case CoursesPackage.TT_ENTRY__ROOM:
			setRoom(ROOM_EDEFAULT);
			return;
		case CoursesPackage.TT_ENTRY__TYPE:
			setType(TYPE_EDEFAULT);
			return;
		case CoursesPackage.TT_ENTRY__STUDYPROGRAM:
			getStudyprogram().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CoursesPackage.TT_ENTRY__DAY:
			return day != DAY_EDEFAULT;
		case CoursesPackage.TT_ENTRY__TIME:
			return TIME_EDEFAULT == null ? time != null : !TIME_EDEFAULT.equals(time);
		case CoursesPackage.TT_ENTRY__ROOM:
			return ROOM_EDEFAULT == null ? room != null : !ROOM_EDEFAULT.equals(room);
		case CoursesPackage.TT_ENTRY__TYPE:
			return type != TYPE_EDEFAULT;
		case CoursesPackage.TT_ENTRY__STUDYPROGRAM:
			return studyprogram != null && !studyprogram.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (day: ");
		result.append(day);
		result.append(", time: ");
		result.append(time);
		result.append(", room: ");
		result.append(room);
		result.append(", type: ");
		result.append(type);
		result.append(')');
		return result.toString();
	}

} //ttEntryImpl
