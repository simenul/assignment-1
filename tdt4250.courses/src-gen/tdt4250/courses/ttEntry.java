/**
 */
package tdt4250.courses;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>tt Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.courses.ttEntry#getDay <em>Day</em>}</li>
 *   <li>{@link tdt4250.courses.ttEntry#getTime <em>Time</em>}</li>
 *   <li>{@link tdt4250.courses.ttEntry#getRoom <em>Room</em>}</li>
 *   <li>{@link tdt4250.courses.ttEntry#getType <em>Type</em>}</li>
 *   <li>{@link tdt4250.courses.ttEntry#getStudyprogram <em>Studyprogram</em>}</li>
 * </ul>
 *
 * @see tdt4250.courses.CoursesPackage#getttEntry()
 * @model
 * @generated
 */
public interface ttEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Day</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.courses.Day}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Day</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Day</em>' attribute.
	 * @see tdt4250.courses.Day
	 * @see #setDay(Day)
	 * @see tdt4250.courses.CoursesPackage#getttEntry_Day()
	 * @model required="true"
	 * @generated
	 */
	Day getDay();

	/**
	 * Sets the value of the '{@link tdt4250.courses.ttEntry#getDay <em>Day</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Day</em>' attribute.
	 * @see tdt4250.courses.Day
	 * @see #getDay()
	 * @generated
	 */
	void setDay(Day value);

	/**
	 * Returns the value of the '<em><b>Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time</em>' attribute.
	 * @see #setTime(String)
	 * @see tdt4250.courses.CoursesPackage#getttEntry_Time()
	 * @model required="true"
	 * @generated
	 */
	String getTime();

	/**
	 * Sets the value of the '{@link tdt4250.courses.ttEntry#getTime <em>Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time</em>' attribute.
	 * @see #getTime()
	 * @generated
	 */
	void setTime(String value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' attribute.
	 * @see #setRoom(String)
	 * @see tdt4250.courses.CoursesPackage#getttEntry_Room()
	 * @model required="true"
	 * @generated
	 */
	String getRoom();

	/**
	 * Sets the value of the '{@link tdt4250.courses.ttEntry#getRoom <em>Room</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' attribute.
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(String value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' attribute.
	 * The literals are from the enumeration {@link tdt4250.courses.Type}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' attribute.
	 * @see tdt4250.courses.Type
	 * @see #setType(Type)
	 * @see tdt4250.courses.CoursesPackage#getttEntry_Type()
	 * @model
	 * @generated
	 */
	Type getType();

	/**
	 * Sets the value of the '{@link tdt4250.courses.ttEntry#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see tdt4250.courses.Type
	 * @see #getType()
	 * @generated
	 */
	void setType(Type value);

	/**
	 * Returns the value of the '<em><b>Studyprogram</b></em>' reference list.
	 * The list contents are of type {@link tdt4250.courses.Studyprogram}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Studyprogram</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Studyprogram</em>' reference list.
	 * @see tdt4250.courses.CoursesPackage#getttEntry_Studyprogram()
	 * @model
	 * @generated
	 */
	EList<Studyprogram> getStudyprogram();

} // ttEntry
